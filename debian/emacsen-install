#! /bin/sh
# /usr/lib/emacsen-common/packages/install/flim
set -e

FLAVOR=$1
PACKAGE=flim

ELCDIR=/usr/share/$FLAVOR/site-lisp/$PACKAGE
ELDIR=/usr/share/$PACKAGE
ELCSTAMP=$ELCDIR/compile-stamp

case $FLAVOR in
    *xemacs*|emacs2[0-3]|emacs1?|mule2)
    exit 0
    ;;
esac

if [ ! -f "/usr/share/$FLAVOR/site-lisp/apel/poem.elc" ]; then exit 0; fi
EMACSEN=$FLAVOR

if [ -f "$ELCSTAMP" ]; then
    if [ -z "`find "$ELDIR/mime.el" -newer "$ELCSTAMP"`" ]; then
	echo "install/$PACKAGE: already byte-compiled for $FLAVOR, skipped"
	exit 0
    fi
    rm -f "$ELCSTAMP"
fi

LOG="$ELCDIR/CompilationLog"
echo "install/$PACKAGE: byte-compiling for $FLAVOR"

cd "$ELDIR"
LINKS=`echo *.el`
if [ ! -d "$ELCDIR" ]; then
    mkdir "$ELCDIR"
    chmod 755 "$ELCDIR"
fi

cd "$ELCDIR"
TOELDIR=../../../$PACKAGE
rm -f *.elc __myinit.el
for f in $LINKS; do
    ln -sf "$TOELDIR/$f" .
done
FILES=`echo *.el | sed s/sasl-xoauth2.el//`

cat > __myinit.el << EOF
(setq inhibit-automatic-native-compilation t)
(setq native-comp-deferred-compilation nil)
(setq comp-enable-subr-trampolines nil)
(setq load-path (cons "/usr/share/$FLAVOR/site-lisp/apel" load-path))
(setq load-path (cons "." load-path))
(setq byte-compile-warnings t)
EOF

FLAGS="-q -no-site-file -batch -l __myinit.el -f batch-byte-compile"
echo "$FLAVOR" $FLAGS $FILES > "$LOG"
set +e
"$FLAVOR" $FLAGS $FILES >> "$LOG" 2>&1
if [ $? -ne 0 ]; then cat "$LOG"; exit 1; fi
set -e
gzip -9f "$LOG"
chmod 644 *.elc

rm -f __myinit.el*

if [ -f /usr/share/emacsen-common/debian-startup.el ] && \
   [ ! -f "/etc/$FLAVOR/site-start.d/50$PACKAGE.el" ] && \
   [ -f "/etc/emacs/site-start.d/50$PACKAGE.el" ] && \
   [ -d "/etc/$FLAVOR/site-start.d" ]; then
    ln -sf "../../emacs/site-start.d/50$PACKAGE.el" "/etc/$FLAVOR/site-start.d/50$PACKAGE.el"
fi

echo "install/$PACKAGE: byte-compiling for reverse dependency"
pkgs=
for p in w3m-el:w3m w3m-el-snapshot:w3m semi mu-cite rail lsdb elserv; do
    pkg=`echo $p | cut -d : -f 1`
    sname=`echo $p | cut -d : -f 2`
    dir=/usr/share/$FLAVOR/site-lisp/$sname
    if [ -n "$sname" ] && [ -d "$dir" ] && [ -f "/usr/lib/emacsen-common/packages/install/$pkg" ]; then
	rm -f "$dir"/*.elc "$dir"/*/*.elc "$dir"/*-stamp "$dir"/*/*-stamp
	pkgs="$pkg $pkgs"
    fi
done
for pkg in $pkgs; do
    "/usr/lib/emacsen-common/packages/install/$pkg" "$FLAVOR"
done

touch "$ELCSTAMP"
exit 0
